#ifndef JSONREADER_H
#define JSONREADER_H

#include <QObject>
#include <QJsonObject>
#include <QThread>

class QStringListModel;
class JsonReaderPrivate;
class QThread;

class JsonReader : public QObject
{
    Q_OBJECT

    friend class JsonReaderPrivate;

public:
    explicit JsonReader(QObject *parent = 0);
    JsonReader(QStringListModel *chapterModel);

    QString openFile() const;
    QString version() const;
    int chapterCount() const;
    int verseCount() const;
    QStringListModel *chapterModel() const;

signals:
    void loadChapter(int chapterNumber);
    void loadJsonFile(QString jsonFile);
    void fillModel(QStringListModel *chapterModel);
    void chapterCountChanged();
    void verseCountChanged();
    void versionChanged();
    void openFileChanged();
    void chapterChanged();
    void bookLoaded(QString jsonFile);
    void bookLoaded(bool);

public slots:
    void openBook(QString bookName);
    void openChapter(int chapterNumber);
    void openChapter(QString bookName, int chapterNumber);

protected:
    void constructCommon();
    QString findBookFile(QString bookName);
    static QByteArray stringFromJson(QString fileName);

    JsonReaderPrivate *m_jrPrivate;
    QStringListModel *m_chapterModel;
    QThread *m_asyncThread;

private slots:
    void onPrivateChapterLoaded(bool loaded);
    void onPrivateChapterCountChanged();
    void onPrivateVerseCountChanged();
    void onPrivateVersionChanged();
    void onPrivateOpenFileChanged();
    void onPrivateChapterChanged();
    void onPrivateJsonFileLoaded(bool loaded);
    void onPrivateJsonFileLoaded(QString file);
};

#endif // JSONREADER_H


#ifndef JsonReaderThread_H
#define JsonReaderThread_H

class JsonReaderPrivate : private QObject
{
    Q_OBJECT
    friend class JsonReader;

signals:
    void chapterCountChanged();
    void verseCountChanged();
    void versionChanged();
    void openFileChanged();
    void chapterChanged();
    void jsonFileLoaded(QString fileName);
    void jsonFileLoaded(bool loaded);
    void chapterLoaded(bool loaded);

private:
    explicit JsonReaderPrivate(QObject *parent = 0);
    JsonReaderPrivate(JsonReader *jr);

    JsonReader *m_jr;
    QJsonObject m_rootObject;
    QJsonObject m_chapterObject;
    QString m_openFile;
    QString m_version;
    int m_chapterCount;
    int m_verseCount;

private slots:
    void loadChapter(int chapterNumber);
    void loadJsonFile(QString fileName);
    void fillModel(QStringListModel *model);
};

#endif // JsonReaderThread_H

