#include <QStandardPaths>
#include <QResource>
#include <QIcon>
#include <QFile>
#include <QDir>
#include "jsonreader.h"

#ifdef Q_OS_ANDROID
#include <QGuiApplication>
#include <QQuickView>
#else
#include <QApplication>
#include "mainwindow.h"
#endif

#ifdef IS_DEBUG
#include <QDebug>
#include <QElapsedTimer>
#endif

int main(int argc, char *argv[])
{
#ifdef Q_OS_ANDROID
    QGuiApplication a(argc, argv);
    QQuickView view;
#else
    QApplication a(argc, argv);
    MainWindow *w = new MainWindow;
#endif
    QString rccPath, bibleRcc, imagesRcc, appDataPath;
    QIcon appIcon;

//    a.setOrganizationDomain("BrandonSoft.com");
//    a.setOrganizationName("BrandonSoft");
    a.setApplicationDisplayName("Bible Reader");
    a.setApplicationName("BibleReader");
    a.setApplicationVersion("1.0.1");

#ifdef Q_OS_ANDROID
    qmlRegisterType<JsonReader>("com.brandon.qml", 1, 0, "JsonReader");
#elif defined(Q_OS_LINUX)
    a.setDesktopFileName("bible-reader.desktop");
#endif

#ifdef IS_RELEASE
    Q_UNUSED(rccPath);

    appDataPath = QStandardPaths::standardLocations(QStandardPaths::DataLocation).last() + "/";
    bibleRcc = appDataPath + "rcc/Bible.rcc";
    imagesRcc = appDataPath + "rcc/images.rcc";
#elif IS_DEBUG
    Q_UNUSED(appDataPath);

    qDebug() << "Current working directory: " << QDir::currentPath();

#ifdef Q_OS_ANDROID
#elif defined(Q_OS_LINUX)
    if(Q_LIKELY(QDir::setCurrent("../install"))) {
        rccPath = QDir::currentPath() + "/usr/share/BibleReader/rcc/";
        bibleRcc = rccPath + "Bible.rcc";
        imagesRcc = rccPath + "images.rcc";

        qDebug() << rccPath;
    }
#endif
#endif

#ifdef Q_OS_ANDROID
    if(QFile(":/Main.qml").exists()) {
        view.setSource(QUrl("qrc:/Main.qml"));
        view.showFullScreen();
    }
#else
    if(Q_LIKELY(QFile(bibleRcc).exists()) && Q_LIKELY(!QFile(":/Holy_Bible.json").exists())) {
        if(Q_LIKELY(QResource::registerResource(bibleRcc))) {
#ifdef IS_DEBUG
            qDebug() << bibleRcc << " was loaded as a resource";
#endif
        }
    }

    if(Q_LIKELY(QFile(imagesRcc).exists()) && Q_UNLIKELY(!QFile(":/BibleReader.png").exists())) {
        if(Q_LIKELY(QResource::registerResource(imagesRcc))) {
#ifdef IS_DEBUG
            qDebug() << imagesRcc << " was loaded as a resource";
#endif
        }
    }

    appIcon = QIcon(":/BibleReader.png");
    a.setWindowIcon(appIcon);
    w->showMaximized();
#endif

    return a.exec();
}
