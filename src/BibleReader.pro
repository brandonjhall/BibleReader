#-------------------------------------------------
#
# Project created by QtCreator 2016-12-15T20:23:28
#
#-------------------------------------------------

QT       += core gui qml quick

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG(debug, debug|release) {
    DEFINES+=IS_DEBUG
} else {
    DEFINES+=IS_RELEASE
}

TARGET = BibleReader
TEMPLATE = app

SOURCES += main.cpp \
    mainwindow.cpp \
    jsonreader.cpp

HEADERS  += \
    mainwindow.h \
    jsonreader.h

FORMS += \
    mainwindow.ui

OTHER_FILES += \
    ../resources/BibleFiles/Holy_Bible.json \
    ../resources/BibleFiles/Genesis.json \
    ../resources/BibleFiles/Exodus.json \
    ../resources/BibleFiles/Daniel.json \
    ../resources/BibleFiles/Luke.json \
    ../resources/qml/Main.qml \
    ../README.md \

RESOURCES += \
    ../resources/Images/images.qrc

linux-g++ {
    OTHER_FILES += \
        ../bible-reader.desktop

    CONFIG(debug, debug|release) {
        target.path = ../install/usr/bin/
        rccfiles.path = ../install/usr/share/BibleReader/rcc/
        desktop.path = ../install/usr/share/applications/
        appicon.path = ../install/usr/share/pixmaps/
    } else {
        target.path = /usr/bin/
        rccfiles.path = /usr/share/BibleReader/rcc/
        desktop.path = /usr/share/applications/
        appicon.path = /usr/share/pixmaps
    }
    rccfiles.files = ../resources/BibleFiles/Bible.rcc \
                     ../resources/Images/images.rcc
    desktop.files = ../bible-reader.desktop
    appicon.files = ../resources/Images/BibleReader.png
    rccfiles.commands = rcc -binary ../resources/BibleFiles/Bible.qrc -o ../resources/BibleFiles/Bible.rcc && \
                            rcc -binary ../resources/Images/images.qrc -o ../resources/Images/images.rcc

    INSTALLS += target rccfiles desktop appicon
}

android-g++ {
#    QT += qml quick
    DISTFILES +=
    RESOURCES += \
        ../resources/qml/qml.qrc \
}
