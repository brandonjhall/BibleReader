#include "jsonreader.h"

#ifdef IS_DEBUG
#include <QDebug>
#include <QTime>
#include <QElapsedTimer>
#endif

#include <QStringListModel>
#include <QApplication>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonValue>
#include <QEventLoop>
#include <QTimer>
#include <QFile>

JsonReader::JsonReader(QObject *parent)
    : QObject(parent)
{
    m_chapterModel = new QStringListModel;
    constructCommon();
}

JsonReader::JsonReader(QStringListModel *chapterModel)
    : QObject(0)
{
    m_chapterModel = chapterModel;
    constructCommon();
}

void JsonReader::openBook(QString bookName)
{
    QString jsonFile;

    if(Q_LIKELY(QFile(QString(":/" + bookName + ".json")).exists())) {
        jsonFile = ":/" + bookName + ".json";
    }
    else {
        jsonFile = findBookFile(bookName);

        if(jsonFile.isNull() || jsonFile.isEmpty()) {
            emit bookLoaded(false);
            return;
        }
    }

    emit loadJsonFile(jsonFile);
}

void JsonReader::openChapter(int chapterNumber)
{
    emit loadChapter(chapterNumber);
}

void JsonReader::openChapter(QString bookName, int chapterNumber)
{
    QEventLoop loop;
    QTimer timer;

    connect(m_jrPrivate, SIGNAL(jsonFileLoaded(QString)), &loop, SLOT(quit()));
    connect(&timer, SIGNAL(timeout()), &loop, SLOT(quit()));
    openBook(bookName);
    timer.start(2000);

    while(loop.exec()) {
        QApplication::processEvents(QEventLoop::AllEvents, 1);
    }

    if(m_jrPrivate->m_openFile.contains(bookName)) {
        emit loadChapter(chapterNumber);
    }

#ifdef IS_DEBUG
    qDebug() << m_jrPrivate->m_openFile;
#endif
}

void JsonReader::constructCommon()
{
    QThread *thread = new QThread;
    m_jrPrivate = new JsonReaderPrivate(this);

    m_jrPrivate->moveToThread(thread);
    connect(this, SIGNAL(loadJsonFile(QString)), m_jrPrivate, SLOT(loadJsonFile(QString)));
    connect(this, SIGNAL(loadChapter(int)), m_jrPrivate, SLOT(loadChapter(int)));
    connect(this, SIGNAL(fillModel(QStringListModel*)), m_jrPrivate, SLOT(fillModel(QStringListModel*)));
    connect(m_jrPrivate, SIGNAL(chapterChanged()), this, SLOT(onPrivateChapterChanged()));
    connect(m_jrPrivate, SIGNAL(chapterCountChanged()), this, SLOT(onPrivateChapterCountChanged()));
    connect(m_jrPrivate, SIGNAL(openFileChanged()), this, SLOT(onPrivateOpenFileChanged()));
    connect(m_jrPrivate, SIGNAL(jsonFileLoaded(bool)), this, SLOT(onPrivateJsonFileLoaded(bool)));
    connect(m_jrPrivate, SIGNAL(jsonFileLoaded(QString)), this, SLOT(onPrivateJsonFileLoaded(QString)));
    connect(m_jrPrivate, SIGNAL(verseCountChanged()), this, SLOT(onPrivateVerseCountChanged()));
    connect(m_jrPrivate, SIGNAL(versionChanged()), this, SLOT(onPrivateVersionChanged()));
    connect(m_jrPrivate, SIGNAL(chapterLoaded(bool)), this, SLOT(onPrivateChapterLoaded(bool)));
    connect(thread, SIGNAL(finished()), m_jrPrivate, SLOT(deleteLater()));
    thread->start();
}

QString JsonReader::findBookFile(QString bookName)
{
    QString fileName;
    QJsonObject rootObject;
    QJsonObject oldTestament;
    QJsonObject newTestament;
    QJsonArray bookList;
    QJsonValue value;
    QJsonDocument document = QJsonDocument::fromJson(stringFromJson(":/Holy_Bible.json"));

    if(Q_UNLIKELY(document.isEmpty() || document.isNull())) {
        return NULL;
    }

    rootObject = document.object();
    value = rootObject.value(QString("Old Testament"));

    if(Q_UNLIKELY(!value.isObject())) {
        return NULL;
    }
    else {
        oldTestament = value.toObject();
    }

    value = oldTestament.value(QString("Book List"));

    if(Q_UNLIKELY(!value.isArray())) {
        return NULL;
    }
    else {
        bookList = value.toArray();
    }

    if(bookList.contains(QJsonValue(bookName)) || bookList.contains(QJsonValue(bookName.toLower()))
            || bookList.contains(QJsonValue(bookName.toUpper()))) {
        int index = bookList.toVariantList().indexOf(bookName);

        value = oldTestament.value(QString("Books"));

        if(Q_UNLIKELY(!value.isArray())) {
            return NULL;
        }

        value = value.toArray().at(index);

        if(Q_UNLIKELY(!value.isObject())) {
            return NULL;
        }

        value = value.toObject().value(bookName);

        if(Q_LIKELY(value.isString())) {
            fileName = value.toString();
        }
        else {
            return NULL;
        }
    }
    else {
        value = rootObject.value(QString("New Testament"));

        if(Q_UNLIKELY(!value.isObject())) {
            return NULL;
        }
        else {
            newTestament = value.toObject();
        }

        value = newTestament.value(QString("Book List"));

        if(Q_UNLIKELY(!value.isArray())) {
            return NULL;
        }
        else {
            bookList = value.toArray();
        }

        if(bookList.contains(QJsonValue(bookName))  || bookList.contains(QJsonValue(bookName.toLower()))
                || bookList.contains(QJsonValue(bookName.toUpper()))) {
            int index = bookList.toVariantList().indexOf(bookName);

            value = newTestament.value(QString("Books"));

            if(Q_UNLIKELY(!value.isArray())) {
                return NULL;
            }

            value = value.toArray().at(index);

            if(Q_UNLIKELY(!value.isObject())) {
                return NULL;
            }

            value = value.toObject().value(bookName);

            if(Q_LIKELY(value.isString())) {
                fileName = value.toString();
            }
            else {
                return NULL;
            }
        }
        else {
            return NULL;
        }
    }

    return fileName;
}

QByteArray JsonReader::stringFromJson(QString fileName)
{
    QString str_file;
    QFile file;

    file.setFileName(fileName);

    if(Q_LIKELY(file.open(QIODevice::ReadOnly | QIODevice::Text))) {
        str_file = file.readAll();
        file.close();

        return str_file.toUtf8();
    }

    return NULL;
}

void JsonReader::onPrivateChapterLoaded(bool loaded)
{
    if(loaded) {
        emit fillModel(m_chapterModel);
    }
}

int JsonReader::verseCount() const
{
    return m_jrPrivate->m_verseCount;
}

QStringListModel *JsonReader::chapterModel() const
{
    return m_chapterModel;
}

int JsonReader::chapterCount() const
{
    return m_jrPrivate->m_chapterCount;
}

QString JsonReader::version() const
{
    return m_jrPrivate->m_version;
}

QString JsonReader::openFile() const
{
    return m_jrPrivate->m_openFile;
}

void JsonReader::onPrivateChapterCountChanged()
{
    emit chapterCountChanged();
}

void JsonReader::onPrivateVerseCountChanged()
{
    emit verseCountChanged();
}

void JsonReader::onPrivateVersionChanged()
{
    emit versionChanged();
}

void JsonReader::onPrivateOpenFileChanged()
{
    emit openFileChanged();
}

void JsonReader::onPrivateChapterChanged()
{
    emit chapterChanged();
}

void JsonReader::onPrivateJsonFileLoaded(bool loaded)
{
    emit bookLoaded(loaded);
}

void JsonReader::onPrivateJsonFileLoaded(QString file)
{
    emit bookLoaded(file);
}






JsonReaderPrivate::JsonReaderPrivate(QObject *parent) : QObject(parent)
{
}

JsonReaderPrivate::JsonReaderPrivate(JsonReader *jr) : QObject(0), m_jr(jr)
{
}

void JsonReaderPrivate::loadJsonFile(QString fileName)
{
#ifdef IS_DEBUG
    QElapsedTimer timer;
    timer.start();
#endif
    int oldChapters = m_chapterCount;
    QJsonValue version;
    QJsonValue totalChapters;
    QJsonDocument document = QJsonDocument::fromJson(JsonReader::stringFromJson(fileName));
    QString bytes = JsonReader::stringFromJson(fileName);
    Q_UNUSED(bytes)

    if(document.isEmpty() || document.isNull()) {
        emit jsonFileLoaded(false);
        return;
    }

    m_rootObject = document.object();
    version = m_rootObject.value(QString("Version"));
    totalChapters = m_rootObject.value(QString("Chapter Count"));

    if(Q_UNLIKELY(!version.isString())) {
        emit jsonFileLoaded(false);
        return;
    }

    if(Q_UNLIKELY(m_version != version.toString())) {
        m_version = version.toString();
        emit versionChanged();
    }


    if(Q_UNLIKELY(totalChapters.toInt() == 0)) {
        emit jsonFileLoaded(false);
        return;
    }
    else {
        m_chapterCount = totalChapters.toInt();
    }

    if(Q_LIKELY(m_chapterCount != oldChapters)) {
        emit chapterCountChanged();
    }

    if(Q_LIKELY(m_openFile != fileName)) {
        m_openFile = fileName;
        emit openFileChanged();
    }

    emit jsonFileLoaded(fileName);
    emit jsonFileLoaded(true);
#ifdef IS_DEBUG
    qDebug() << Q_FUNC_INFO << "finished";
    qDebug() << "Time to load jsonFile: " << timer.elapsed();
#endif
}

void JsonReaderPrivate::fillModel(QStringListModel *model)
{
    QStringList stringList;
    QJsonArray versesArray = m_chapterObject.value(QString("Verses")).toArray();

    for(int i = 0; i < m_verseCount; i++) {
        QJsonObject verse = versesArray.at(i).toObject();
        stringList.append(verse.value(QString::number(i + 1)).toString());
    }

    model->setStringList(stringList);
#ifdef IS_DEBUG
    qDebug() << Q_FUNC_INFO << "stringList: " << stringList;
#endif
}

void JsonReaderPrivate::loadChapter(int chapterNumber)
{
#ifdef IS_DEBUG
    QElapsedTimer timer;
    timer.start();
#endif
    QJsonValue qjv_verseCount;

    if(Q_UNLIKELY(chapterNumber > m_chapterCount)) {
        emit chapterLoaded(false);
        return;
    }
    else {
        QJsonValue chaptersValue = m_rootObject.value("Chapters");
        QJsonValue chapterValue;
        QJsonValue arrayValue;
        QJsonArray chaptersArray;
        QJsonObject arrayObject;

        if(Q_UNLIKELY(!chaptersValue.isArray())) {
            emit chapterLoaded(false);
            return;
        }
        else {
            chaptersArray = chaptersValue.toArray();
        }

        arrayValue = chaptersArray.at(chapterNumber - 1);

        if(Q_UNLIKELY(!arrayValue.isObject())) {
            emit chapterLoaded(false);
            return;
        }
        else {
            arrayObject = arrayValue.toObject();
        }

        chapterValue = arrayObject.value(QString::number(chapterNumber));

        if(Q_UNLIKELY(!chapterValue.isObject())) {
            emit chapterLoaded(false);
            return;
        }
        else {
            m_chapterObject = chapterValue.toObject();
        }
    }

    qjv_verseCount = m_chapterObject.value("Verse Count");

    if(Q_UNLIKELY(qjv_verseCount.toInt() == 0)) {
        emit chapterLoaded(false);
        return;
    }
    else {
        m_verseCount = qjv_verseCount.toInt();
        emit verseCountChanged();
    }

    emit chapterLoaded(true);

#ifdef IS_DEBUG
    qDebug() << Q_FUNC_INFO << "finished";
    qDebug() << "Time to load chapter: " << timer.elapsed();
#endif
}
