## BibleReader

**Project Info**
Project: BibleReader,
Author: Brandon Hall,
Project Start Date: December 15, 2016

## Building and Running:

When you first download this project, you will need to add an argument to make in qtcreator Debug.
To do this in qtcreator, open the project and click on Projects on the left vertical toolbar,
change the current build configuration to debug, go to "Build Steps"->Make, expand the
details for make, and add a "Make arguments:" of "install".
